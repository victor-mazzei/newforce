<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newforce
 */

?>

<div class="row footer">
  <div class="container">
      <div class="col-md-4 m-top">
        <div id="footerSaibaMais">
            <span class="saiba-text">Sobre a New Force</span>
            <p>A <span>NEW FORCE</span> é uma empresa de engenharia com foco em soluções de montagem e
              manutenção eletromecânica industrial. Atua em todo o terrítório nacional</p>
            <a href="<?php echo site_url()?>/sobre">Saiba mais..</a>
          </div>
      </div>
      <div class="col-md-4 m-top">
        <div id="footerFaleConosco">
          <span class="saiba-text">Fale Conosco</span>

          <?php echo do_shortcode('[contact-form-7 id="45" title="Formulário Footer"]') ?>
           
          </div>
      </div>
      <div class="col-md-4 m-top">
          <div id="footerLocal">
              <span class="saiba-text">Contato</span><br>
              <div class="linha">
                <span class="fa fa-map-marker local"><span>Av. Senador Feijó, 686. cjs 623 <br> Vila Mathias, Santos - SP</span></span><br>
              </div>
              <div class="linha">
                <span class="fa fa-mobile telefone"><span>+55 13 99118-1524 <br>
                    +55 13 3202-7534</span></span><br>
              </div>
              <div >
                <span class="fa fa-paper-plane-o contato" ><span>contato@newforce.ind.br</span></span><br>
              </div>
          </div>
      </div>
  </div>
</div>
  
<?php wp_footer(); ?>

  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
