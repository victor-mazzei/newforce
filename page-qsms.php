 <?php
  get_header();
?> 

<div class="container">
	<div class="row">
		<div class="qsmsBody">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div id="qsmsTitulo">
                  <h3>QSMS</h3>
                  <hr>
               </div>
               <div class="qsmsDescricao">
                  <div class="row">
                     <div class="col-md-12 text-center">

                        <div class="col-md-2" >
                           <img src="<?php echo get_template_directory_uri() .'/public/images/qualidade.png'?>" alt="Qualidade" >
                           <span> <strong>QUALIDADE</strong> </span>
                        </div>
                        <div class="col-md-2">
                           <img src="<?php echo get_template_directory_uri() .'/public/images/seguranca.png'?>" alt="Segurança">
                           <span> <strong>SEGURANÇA</strong></span>
                        </div>
                        <div class="col-md-2">
                           <img src="<?php echo get_template_directory_uri() .'/public/images/meio_ambiente.png'?>" alt="Meio Ambiente"> <br>
                           <span> <strong>MEIO AMBIENTE</strong></span>
                        </div>
                        <div class="col-md-2" >
                           <img src="<?php echo get_template_directory_uri() .'/public/images/hospital.png'?>" alt="Hospital"><br>
                           <span> <strong>SAÚDE</strong></span>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <p>A NEWFORCE, entende que a qualidade é de vital importância para assegurar o desenvolvimento da empresa
                           executa obras e serviços. A responsabilidade por QSMS - Qualidade, Segurança, Meio Ambiente e
                           Saúde - é de todos e estã presente em nossas atividades, atitudes e valores: Orientação para Resultados,
                           Competência Profissional, Garra e Confiança.
                        </p>
                     </div>
                  </div>
               </div>

               <div class="row">
                  <div class="container qsms">
                     <div class="qsmsHero center">
                        <div class="col-md-10">
                           <h3><strong>Nossos Compromissos</strong></h3>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Satisfazer as expectativas dos clientes;</span>                              </div>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Garantir a qualidade dos serviços, atendendo aos requisitos legais, normas e especificações aplicáveis;</span>                              </div>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Prevenir riscos à segurança e preservar a saúde dos trabalhadores;</span>                              </div>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Respeitar o meio ambiente e prevenir a poluição;</span>                              </div>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Promover o desenvolvimento e o bem-estar dos trabalhadores através de ações de responsabilidade social;</span>                              </div>
                           <div><i class="fa fa-check" aria-hidden="true"></i> <span>Buscar a melhoria contínua do Sistema de Gestão Integrada de QSMS.</span>                              </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>


      </div>
   </div>
	</div>
</div>
  
<?php 

get_footer();
 ?>