 <?php
get_header();
?>
<div class="container">
    
</div>
 <div class="row">
    <div class="col-md-12">
        <div class="tituloPagina">
            <h3>CONTATO</h3>
            <hr>
        </div>
    </div>
</div>
<div class="row">
    <div class="container">
        <div class="containerContato">
            <div class="col-md-6">
                <div class="contatoForm">
                    <div class="form-area">
                        <div class="row">
                            <p id="campos">
                                *Campos Obrigatórios
                            </p>
                        </div>
                        <div class="row">
                             <?php echo do_shortcode('[contact-form-7 id="46" title="Contato"]') ?>
                        </div>

                    </div>
                </div>
            </div>
            <div id="contatoInfo" class="col-md-6">
               
                    <div class="linha">
                            <span class="fa fa-map-marker local"><span>Av. Senador Feijó, 686. cjs 623 
<br> Vila Mathias, Santos - SP</span></span><br>
                    </div>
                    <div class="linha">
                          <div class="linha">
                            <span class="fa fa-mobile telefone"><span>+55 13 99118-1524 <br>
+55 13 3202-7534</span></span><br>
                        </div>
                    </div>
                    <div class="linha">
                        <span class="fa fa-paper-plane-o contato"><span>contato@newforce.ind.br</span></span><br>
                    </div>
                
            </div>
        </div>
    </div>
</div>



             <?php 

get_footer();
 ?>