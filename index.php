<?php
  get_header();
?>

    <div class="row">
        <div class="col-md-12">
            <?php   get_template_part( 'template-parts/content', 'slider' ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php   get_template_part( 'template-parts/content', 'servicos' ); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="bg-servicos" style="background-image: url(<?php echo get_template_directory_uri() . '/public/images/bg_sobre.png'; ?>)">

                <div class="text-services" style="background-image: url(<?php echo get_template_directory_uri() . '/public/images/box_bg_sobre.png'; ?>)">
                    <div class="main-content-services">
                        <p class="main-text">Sobre a New Force</p>
                        <p class="sub-text">A <span style="color:#2d7178;">NEW FORCE</span> é uma empresa de engenharia com foco em soluções de montagem e manutenção eletromecânica industrial</p>
                        <p class="min-text">
                            Há mais de 03 anos presente no mercado, se tornou uma referência nacional em obras industriais. Executa importantes projetos de implantação, ampliação, modernização e manutenção nas indústrias em todo o Brasil.
                        </p>
                        <a href="<?php echo site_url()?>/sobre" class="btn btn-lg btn-serv">Saiba Mais</a>
                    </div>
                </div>
            </div>
            <div id="bg-servicos-mob">
                <h2 class="main">Sobre a New Force</h2>
                <h3 class="sub">A <span style="color:#2d7178;">NEW FORCE</span> é uma empresa de engenharia com foco em soluções de montagem e manutenção eletromecânica industrial</h3>
                <h4 class="sub">
                    Há mais de 03 anos presente no mercado, se tornou uma referência nacional em obras industriais. Executa importantes projetos de implantação, ampliação, modernização e manutenção nas indústrias em todo o Brasil.
                  </h4>
                <a href="<?php echo site_url()?>/sobre" class="btn  btn-block btn-serv">Saiba Mais</a>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <?php   get_template_part( 'template-parts/content', 'atuacao' ); ?>
      </div>
    </div>

    <div class="row saibaMais">
        <div class="container">
            <div class="col-md-8">
                  <h1>A melhor solução para o seu projeto</h1>
                  <h4>Entre em contato e faça um orçamento pelo email <a href="mailto:contato@newforce.ind.br?Subject=contato" target="_top">contato@newforce.ind.br</a></h4>
            </div>
            <div class="col-md-3">
                <a href="<?php echo site_url()?>/sobre" class="btn btn-more btn-lg btn-block" >Saiba mais</a>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-md-12">
          <?php   get_template_part( 'template-parts/content', 'clientes' ); ?>
      </div>
    </div>

    <?php 

get_footer();
 ?>