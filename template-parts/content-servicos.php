<?php
   /**
    * Template part for displaying page servicos in index.php
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package newforce
    */
   
   ?>



<div id="servicosOferecidos">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="main-text">
          <p class="main">Serviços Oferecidos</p>
          <p class="sub">Uma plataforma integrada de serviços oferecendo soluções de engenharia para os mais diversos setores, grandes obras industriais e de infraestrutura.</p>
        </div>
      </div>
      
    </div>
    <div class="row">
        <?php
        
         $args = array(
           'post_type' => 'servicos',
           'posts_per_page'=>3
         );
         $the_query = new WP_Query( $args );
         
         if ( $the_query->have_posts() ) {


           while ( $the_query->have_posts() ) {
            $the_query->the_post(); 

             $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

            ?>

          <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="media">
              <div class="media-left">
                <div class="box-img" style="background: url('<?php echo esc_url($featured_img_url) ?>')">
                    
                </div>
                <img src="" class="media-object" >
              </div>
               <div class="media-body">
                  <h4 class="media-heading"><?php the_title();  ?></h4>
                  <p><?php the_field('descricao'); ?></p>
                  <p class="saibaMaisTexto"><a href="<?php echo site_url()?>/servicos">Saiba Mais..</a></p>
                </div>
            </div>
          </div>
          <?php } }?>
    </div>
  </div>
</div>