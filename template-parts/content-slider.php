<?php
   /**
    * Template part for displaying page slider in index.php
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package newforce
    */
   
   ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
   <!-- Indicators -->
   <!-- Wrapper for slides -->
   <div class="carousel-inner">
      <?php
         $count = 0;
         $args = array(
           'post_type' => 'slider',
         );
         $the_query = new WP_Query( $args );
         
         if ( $the_query->have_posts() ) {
           while ( $the_query->have_posts() ) {
            $the_query->the_post(); ?>
      <?php if($count == 0):?>
      <div class="item active">
         <?php else:?>
         <div class="item">
            <?php endif;?>
            <?php the_post_thumbnail(); ?>
            <div class="carousel-caption">
               <h1><?php the_title(); ?></h1>
               <h4><?php the_excerpt(); ?></h4>
            </div>
         </div>
         <?php
            $count++;
            }
            wp_reset_postdata();
            }else {}
            ?>
         <ol class="carousel-indicators">
            <?php for($i = 0; $i < $count; $i++){?>
            <?php if($i == 0): ?>
            <li class="active" data-target="#myCarousel" data-slide-to="<?php echo $i?>"></li>
            <?php else: ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i?>"></li>
            <?php endif; ?>
            <?php } ?>
         </ol>
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
      </a>
   </div>
</div>