<?php
   /**
    * Template part for displaying page atuacao in index.php
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package newforce
    */
   
   ?>

<div id="atuacao">

	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-11">
				<div class="atuacao-text">
					<h1 class="main-a">Áreas de Atuação</h1>
    				<p class="sub">Atua em todo território nacional prestando serviços em <br>diversos segmentos.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-xs-12 col-md-offset-1 col-md-5">
				<div class=" col-sm-6 col-xs-6 col-md-6 ">
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Alimentação e Bebidas</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Automobilistica</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Cimento</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Embalagens</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Energia</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Engenharia e Tecnologia</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Fabricação</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Infraestrutura</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Manutenção</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Mineração</span></span>
			    <br>
			</div>

			<div class="col-sm-6 col-xs-6 col-md-6">
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Montagens industriais</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Naval e Offshore</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Operadora Portuaria</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Óleo e Gás</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Química</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Papel e Celulose</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Petroquímicas</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Siderurgia</span></span>
			    <br>
			    <span class="fa fa-check checkIcon"><span class="checkIconTexto"> Vidro</span></span>
			    <br>
			</div>
			</div>
			<div class="col-md-6 hide-1180">
				<img  class="img-atuacao" src="<?php echo get_template_directory_uri() . '/public/images/atuacao-img.png'; ?>" alt="Atuação">
			</div>
		</div>
	</div>
	
</div>
    