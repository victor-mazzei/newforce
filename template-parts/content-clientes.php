<?php
   /**
    * Template part for displaying page clientes in index.php
    *
    * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
    *
    * @package newforce
    */
   
   ?>

    <div class="container" id="clientes">
        <div class="row">
            <div class="col-md-12">

                <?php 
                if(is_home()){
                    $args = array(
                        'post_type' => 'clientes',
                        'posts_per_page' => 8,
                        'orderby' => 'title',
                        'order'   => 'ASC',
                      );

                ?>
                 <div class="main-text">
                    <h1 class="main">Nossos Clientes</h1>
                    <p class="sub">A melhor forma de conhecer a qualidade dos nossos serviços é por meio daqueles com quem trabalhamos.</p>
                </div>

                <?php    
                    }else{
                    $args = array(
                        'post_type' => 'clientes',
                        'orderby' => 'title',
                        'order'   => 'ASC',
                      );
              
                ?>

                <div class="tituloPagina">
                    <h3>CLIENTES</h3>
                    <hr>
                    
                </div>
                <div class="descricao">
                        A melhor forma de conhecer a qualidade dos nossos serviços é por meio daqueles com quem trabalhamos.
                    </div>
                <?php 


                } ?>

               
                
            </div>
        </div>
        <div class="row">
            

                <?php

               
			      
			      $the_query = new WP_Query( $args );

			      if ( $the_query->have_posts() ) {
			        while ( $the_query->have_posts() ) {
			          $the_query->the_post(); 

                       $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),array(270,70));

                      ?>
                    <div class="col-xs-6 col-sm-4 col-md-3">
          
			           <img class="img-responsive img-set" src="<?php echo $featured_img_url ?>" alt="<?php the_title() ?>">
                     </div>

			                    <?php
			        }
			        wp_reset_postdata();
			      }
			      ?>

           
        </div>
        
    </div>
       
