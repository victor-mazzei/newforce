<?php
  get_header();
?>

    <div class="row">
        <div class="col-md-12">
            <div class="tituloPagina">
                <h3>TRABALHE CONOSCO</h3>
                <hr>
            </div>
        </div>
    </div>

    <div class="container containerContato">
        <div class="col-md-5 contatoForm">
            <div class="form-area">

                <div class="row">
                    <p id="campos">*Campos Obrigatórios</p>
                </div>
                <div class="row">
                    <?php echo do_shortcode('[contact-form-7 id="47" title="Formulario RH"]') ?>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Vagas Disponiveis</h3>
                    <p>Clique para ver os detalhes da vaga</p>
                </div>
                <div class="list-group">
                    <?php 

                     $args = array(
                       'post_type' => 'vagas',
                     );
                     $the_query = new WP_Query( $args );

                     if ( $the_query->have_posts() ) {

                       while ( $the_query->have_posts() ) {
                        $the_query->the_post(); 

                    ?>
                        <a data-toggle="collapse" href="#<?php echo get_the_ID(); ?>" class="list-group-item" aria-expanded="false" aria-controls="<?php echo get_the_ID() ?>">
                            <?php the_title(); ?>
                        </a>
                        <div class="collapse" id="<?php echo get_the_ID(); ?>">
                            <div class="vaga-desc">
                                <?php the_content(); ?>
                            </div>
                        </div>

                        <?php
                        }
                    }else{
                    ?>
                          <a href="#" class="list-group-item text-center">Ainda não temos vagas cadastradas</a>
                            <?php 
                        }
                     ?>
                               
                </div>
            </div>
        </div>
    </div>

    <?php 

get_footer();
 ?>