 <?php
  get_header();
?>
 <div class="eticaBody">
      <div class="col-md-12">
         <div class="row">
            <div class="col-md-12">
               <div class="tituloPagina">
                  <h3>ÉTICA</h3>
                  <hr>
               </div>
               <div class="eticaDescricao">
                  <div class="row">
                     <div class="col-md-12 text-justify">
                        <p>A empresa acredita que é de extrema importância construir e manter um ambiente favorável, baseado
                           em uma cultura ética e transparente, no qual as pessoas depositem orgulho, integridade e dedicação.
                        </p>
                        <p>o Código de Conduta da NEWFORCE estabelece e dissemina regras, princípios e valores que regulam sua
                           atuação no mercado, bem como guiam a ação profissional de todos os seus colaboradores e parceiros,
                           a fim de manter o bom desempenho de suas atividades e o relacionamento harmonioso entre os públicos
                           interno e externo, colaboradores, clientes e terceirizado.
                        </p>
                        <p>Todos os colaboradores, diretores e quaisquer representantes da NEWFORCE, devem estar cientes e agir
                           de acordo com estes princípios em todas as funções e atividades do dia a dia e tendo-os como norte
                           para convívio entre si, e a todos os fornecedores, os quais devem respeitar e cumprir as regras
                           de conduta descritas no código para manterem uma relação comercial com a NEWFORCE.
                        </p>
                     </div>

                  </div>
               </div>

               <div class="row">
                     <div class="container etica">
                       <div class="eticaHero center">
                         <div class="col-md-10">
                           <p>O <strong>Código de Conduta da NEWFORCE</strong> estabelece e dissemina regras, princípios e valores que
                            regulam sua atuação no mercado
                           </p>
                         </div>
                       </div>
                     </div>
                   </div>
            </div>
         </div>


      </div>
   </div>
    
  <?php 

get_footer();
 ?>