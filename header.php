<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package newforce
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <title>NEW FORCE - Inovando Sempre</title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/public/images/favicon.ico'?>" type="image/x-icon" />

	  <!-- Bootstrap CSS -->
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/public/css/main.css?v=' . microtime() ?>" alt="" >

</head>

<body >
		
	<nav class="navbar navbar-default navbar-static-top" >
      <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false"
               aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
            <a class="navbar-brand" href="<?php echo site_url()?>">
          <!-- <img src="img/Logo - New Force.png" alt="" ></a> -->
          <img class="img-responsive" src="<?php echo get_template_directory_uri() . '/public/images/logo-newForce.png'?>" alt="Logo New Force"></a>
         </div>
         <div id="navbar" class="navbar-collapse collapse" >
            <ul class="nav navbar-nav navbar-right">
               <li><a href="<?php echo site_url()?>">HOME</a></li>
               <li><a href="<?php echo site_url()?>/sobre">EMPRESA</a></li>
               <li><a href="<?php echo site_url()?>/qsms">QSMS</a></li>
               <li><a href="<?php echo site_url()?>/rh">RH</a></li>
              <!--  <li><a href="<?php echo site_url()?>/premios">PRÊMIOS</a></li> -->
               <li><a href="<?php echo site_url()?>/clientes">CLIENTES</a></li>
               <li><a href="<?php echo site_url()?>/obras">OBRAS</a></li>
               <li><a href="<?php echo site_url()?>/servicos">SERVIÇOS</a></li>
               <li><a href="<?php echo site_url()?>/etica">ÉTICA</a></li>
               <li><a href="<?php echo site_url()?>/contato">CONTATO</a></li>
            </ul>
         </div>
      </div>
   </nav>
