<?php
  get_header();
?>
<div class="container">
  <div class="row">
      <div class="sobreBody">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="tituloPagina">
            <h3>SOBRE A NEW FORCE</h3>
            <hr>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row center">
            <div class="col-md-5 center">
              <div class="about">
                <p>A New Force é uma empresa de engenharia com foco em soluções de montagem e manutenção eletromecânica
                  industrial. Atua em todo o terrítório nacional e presta serviços a diversos segmentos industriais, papel
                  e celulose, offshore, siderurgia e metalurgia, cimento e cal, alimentício, embalagens, têxtil, infraestrutura,
                  petro-química, automobilística, petróleo e gás e química.</p>
                <p>Há mais de 3 anos presente no mercado, a New Force se tornou uma referência nacional em obras industriais. Executa importantes projetos de implatação, ampliação, modernização e manutenção nas industrias em todo
                  o Brasil. Fundada em 2014, é reconhecida pela sua capacidade de realização. Herdou o Know-how de engenharia
                  construtiva do Brasil.</p>
              </div>
            </div>
            <div class="col-md-5 center">
              <div class="col-md-12 box" id="estrutura">
                <div class="col-md-6 center estruturaFoto">

                </div>
                <div class="col-md-6 center estruturaInfo">
                  <div class="row estruturaInfra">
                    <h3>Estrutura da Empresa </h3>
                    <p>O escritório está localizado na cidade de Santos em um ambiente agradável e aconchegante para atender
                      nossos clientes.</p>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="container">
          <div class="jumbotron center" style="margin:50px 0;">
            <div class="col-md-4">
              <h3>Nossa Missão</h3>
              <p>Nossa missão é oferecer soluções de engenharia para implatação, ampliação e manutenção industrial, atendendo
                às expectativas das partes interessadas, contribuindo para o desenvolvimento industrial do país.
              </p>
            </div>
            <div class="col-md-4">
              <h3>Nossa Visão</h3>
              <p>Ser uma empresa de referência nacional em seu segmento de atuação, tendo a preferência dos clientes e colaboradas
                em suas escolhas.
              </p>
            </div>
            <div class="col-md-4">
              <h3>Nosso Valor</h3>
              <p>Para dar sustentação à nossa visão, delineamos nossos valores básicos, que são a essência do negócio e que
                orientam a conduta de todos que fazem da New Force, criando uma base confiável nas relações com clientes,
                fornecedores e colaboradores.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="tituloPagina">
            <h3>SEGURANÇA NO TRABALHO</h3>
            <hr>
          </div>
          <div class="servicosDescricacao">
            <span></span>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="row center" id="seguranca">
            <div class="col-md-4 center segurancaFoto">

            </div>
            <div class="col-md-4 center segurancaInfo">
              <div class="row">
                <p>A <a href="">New Force</a> tem o compromisso em proteger a saúde e a SEGURANÇA de seus funcionários, a qualidade
                  de seus serviços, do ambiente operacional e do meio ambiente.</p>
                <p>Preocupa-se em orientar os colaboradores quanto aos possíveis riscos de suas atividades, uso correto de EPI,
                  no seu ambiente de trabalho, através de DDS (diálogo diário de segurança), regulamentos internos, integrações
                  e treinamentos.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  </div>
</div>
 
  <?php 

get_footer();
 ?>