    <?php
  get_header();
?>
<div class="container">
  <div class="row">
      <div class="col-md-12">
        <div class="servicosBody">
          <div class="tituloPagina">
            <h3>SERVIÇOS</h3>
            <hr>
          </div>
          <div class="servicosDescricao">
            <span></span>
          </div>
        </div> 
      </div>
  </div>
 <div class="row">
 <?php
        
         $args = array(
           'post_type' => 'servicos',
         );
         $the_query = new WP_Query( $args );
         
         if ( $the_query->have_posts() ) {


           while ( $the_query->have_posts() ) {
            $the_query->the_post(); 

             $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

            ?>
 
    <div class="col-xs-12 col-sm-6 col-md-3">
      <div class="thumbnail">
        <img class="img-reponsive" src="<?php echo $featured_img_url ?>">
        <div class="caption">
          <h3 class="text-center"><?php the_title(); ?></h3>
          <p class="text-justify"><?php the_field('descricao'); ?></p>
          <p class="text-center"><a data-toggle="modal" data-target="#<?php echo get_the_ID(); ?>" class="btn btn-primary btn-sm">Ver Mais</a></p>
        </div>
      </div>
    </div>


  <!-- MODAL -->

    <div class="modal fade" id="<?php echo get_the_ID() ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel"><?php the_title(); ?></h4>
      </div>
      <div class="modal-body">
        <?php the_content(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">FECHAR</button>
        
      </div>
    </div>
  </div>
</div>



   <?php } }?>
     </div>

</div>
     <?php 

get_footer();
 ?>